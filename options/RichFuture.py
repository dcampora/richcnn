###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# --------------------------------------------------------------------------------------

from Gaudi.Configuration import *
from GaudiConfig.ControlFlow import seq
from Configurables import CondDB, LHCbApp, GaudiSequencer
import os

# --------------------------------------------------------------------------------------


# Add PID information
def addInfo(alg, typ, name):
    t = alg.addTool(typ, name)
    alg.AddInfo += [t]
    return t


# --------------------------------------------------------------------------------------

# Are we running in a batch job ?
batchMode = "CONDOR_FILE_BATCH" in os.environ

# histograms
#histos = "OfflineFull"
histos = "Expert"

# ROOT persistency
ApplicationMgr().HistogramPersistency = "ROOT"
from Configurables import RootHistCnv__PersSvc
RootHistCnv__PersSvc('RootHistCnv').ForceAlphaIds = True
myBuild = os.getenv('User_release_area', 'None').split('/')[-1]
myConf = os.getenv('BINARY_TAG', 'None')
rootFileBaseName = "RichFuture-" + myBuild + "-" + myConf + "-" + histos
HistogramPersistencySvc().OutputFile = rootFileBaseName + "-Histos.root"

# Event numbers
#nEvents = (50000 if not batchMode else 10000000)
#EventSelector().PrintFreq = (100 if not batchMode else 250)
nEvents = 20
EventSelector().PrintFreq = 10

#LHCbApp().SkipEvents      = 616

# Just to initialise
CondDB()
LHCbApp()

# Timestamps in messages
#LHCbApp().TimeStamp = True

msgSvc = getConfigurable("MessageSvc")
#msgSvc.setVerbose += [ "DeRichGasRadiator" ]
#msgSvc.setVerbose += [ "DeRichSystem" ]
#msgSvc.setVerbose += [ "DeRichPMT" ]
#msgSvc.OutputLevel = 1
msgSvc.Format = "% F%40W%S%7W%R%T %0W%M"
from Configurables import SequencerTimerTool
SequencerTimerTool("ToolSvc.SequencerTimerTool").NameSize = 40

# Auditors
AuditorSvc().Auditors += ["FPEAuditor"]
from Configurables import FPEAuditor
FPEAuditor().ActivateAt = ["Execute"]
#AuditorSvc().Auditors += [ "NameAuditor" ]

# The overall sequence. Filled below.
all = GaudiSequencer("All", MeasureTime=True)

# --------------------------------------------------------------------------------------

# Finally set up the application
ApplicationMgr(
    TopAlg=[all],
    EvtMax=nEvents,  # events to be processed
    ExtSvc=['ToolSvc', 'AuditorSvc'],
    AuditAlgorithms=True)

#ApplicationMgr().ExtSvc += ["Gaudi::MetaDataSvc"]

# --------------------------------------------------------------------------------------

# Fetch required data from file
from Configurables import Gaudi__Hive__FetchDataFromFile as FetchDataFromFile
fetcher = FetchDataFromFile('FetchDSTData')
fetcher.DataKeys = ['Trigger/RawEvent', 'Rich/RawEvent', 'pRec/Track/Best']
all.Members += [fetcher]

# First various raw event decodings

from Configurables import createODIN
odinDecode = createODIN("ODINFutureDecode")
all.Members += [odinDecode]

from Configurables import Rich__Future__RawBankDecoder as RichDecoder
richDecode = RichDecoder("RichFutureDecode")
all.Members += [richDecode]

# Explicitly unpack the Tracks

#from Configurables import UnpackTrackFunctional
#tkUnpack = UnpackTrackFunctional("UnpackTracks")
from Configurables import UnpackTrack
tkUnpack = UnpackTrack("UnpackTracks")
all.Members += [tkUnpack]

# Filter the tracks by type

from Configurables import Rich__Future__Rec__TrackFilter as TrackFilter
tkFilt = TrackFilter("TrackTypeFilter")
all.Members += [tkFilt]

# Now get the RICH sequence

# Preload the geometry during initialise. Useful for timing studies.
preloadGeom = True
#preloadGeom = False

# Work around for some tracking tools the RICH uses that doesn't seem
# to be thread safe yet. Does not matter for normal execution, but if
# using Gaudi Hive set this to true. Changes how the RICH works to
# a not completely ideal mode, but avoids the problem.
workAroundTrackTools = False
#workAroundTrackTools = True

# track extrapolator type
#tkExtrap = "TrackRungeKuttaExtrapolator/TrackExtrapolator"
tkExtrap = "TrackSTEPExtrapolator/TrackExtrapolator"

# Input tracks
tkLocs = {
    "Long": tkFilt.OutLongTracksLocation,
    "Down": tkFilt.OutDownTracksLocation,
    "Up": tkFilt.OutUpTracksLocation,
    "Seed": tkFilt.OutSeedTracksLocation
}
#tkLocs = { "All"  : "Rec/Track/Best" }
#tkLocs = {"Long": tkFilt.OutLongTracksLocation}
#tkLocs = { "Up" : tkFilt.OutUpTracksLocation }
#tkLocs = { "Down" : tkFilt.OutDownTracksLocation }

# Output PID
pidLocs = {
    "Long": "Rec/Rich/LongPIDs",
    "Down": "Rec/Rich/DownPIDs",
    "Up": "Rec/Rich/UpPIDs",
    "Seed": "Rec/Rich/SeedPIDs"
}
#pidLocs = { "All" : "Rec/Rich/PIDs" }
#pidLocs = {"Long": "Rec/Rich/LongPIDs"}
#pidLocs = { "Up" : "Rec/Rich/UpPIDs" }
#pidLocs = { "Down" : "Rec/Rich/DownPIDs" }

# RICH radiators to use
rads = ["Rich1Gas", "Rich2Gas"]
#rads = ["Rich1Gas"]
#rads = ["Rich2Gas"]

# PID types to process
parts = [
    "electron", "muon", "pion", "kaon", "proton", "deuteron", "belowThreshold"
]
#parts = ["pion","kaon","proton","deuteron","belowThreshold"]

photonRecoType = "Quartic"
#photonRecoType = "FastQuartic"
#photonRecoType = "CKEstiFromRadius"

photonSel = "Nominal"
#photonSel = "Online"
#photonSel = "None"

# Number ring points for ray tracing (scaled by CK theta)
ringPointsMin = (16, 16, 16)
ringPointsMax = (96, 96, 96)

# Compute the ring share CK theta values
#rSTol = 0.075  # as fraction of sat. CK theta
#newCKRingTol = (rSTol, rSTol, rSTol)
newCKRingTol = (0.0, 0.05, 0.1)

# Detectable Yield calculation
detYieldPrecision = "Average"
#detYieldPrecision = "Full"

# Track CK resolution treatment
#tkCKResTreatment = "Functional"
tkCKResTreatment = "Parameterised"

# CK resolutions
from RichFutureRecSys.ConfiguredRichReco import defaultNSigmaCuts
ckResSigma = defaultNSigmaCuts()
#S = 4.5
#for tk in tkLocs.keys():
#    # Override presel cuts
#    ckResSigma[photonSel][tkCKResTreatment][tk][0] = (S, S, S)
#    # Override sel cuts
#    ckResSigma[photonSel][tkCKResTreatment][tk][1] = (99,99,99)

# Track CK res scale factors
tkCKResScaleF = None  # Use nominal values
#scF = 1.2
#tkCKResScaleF = (scF, scF, scF)

# Merged output
finalPIDLoc = "Rec/Rich/PIDs"

# DataType
#dType = "2016"
dType = "Upgrade"

# Online Brunel mode.
#online = True
online = False

# PD Grouping for backgrounds
pdGroupSize = (4, 4)

# Create missing track states
createStates = False

# Perform clustering ?
pixelClustering = (False, False)
#pixelClustering = ( True, True )

# Truncate CK angles ?
#truncateAngles = (False, False, False)
truncateAngles = (True, True, True)

# Minimum photon probability
minPhotonProb = None  # Nominal

# Likelihood Settings
# Number Iterations
nIts = 2
# likelihood thresholds
likeThres = [-1e-2, -1e-3, -1e-4, -1e-5]  # nominal
#likeThres = [-1e-3, -1e-3, -1e-4, -1e-5]

# OutputLevel (-1 means no setting)
outLevel = -1

# --------------------------------------------------------------------------------------
# The reconstruction
from RichFutureRecSys.ConfiguredRichReco import RichRecoSequence
RichRec = RichRecoSequence(
    OutputLevel=outLevel,
    dataType=dType,
    onlineBrunelMode=online,
    photonReco=photonRecoType,
    photonSelection=photonSel,
    preloadGeometry=preloadGeom,
    radiators=rads,
    particles=parts,
    makeTkToolsThreadSafe=workAroundTrackTools,
    createMissingStates=createStates,
    PDGroupSize=pdGroupSize,
    NRingPointsMax=ringPointsMax,
    NRingPointsMin=ringPointsMin,
    NewCKRingTol=newCKRingTol,
    minPhotonProbability=minPhotonProb,
    detYieldPrecision=detYieldPrecision,
    tkCKResTreatment=tkCKResTreatment,
    tkCKResScaleFactors=tkCKResScaleF,
    nSigmaCuts=ckResSigma,
    applyPixelClustering=pixelClustering,
    truncateCKAngles=truncateAngles,
    trackExtrapolator=tkExtrap,
    nIterations=nIts,
    LikelihoodThreshold=likeThres,
    inputTrackLocations=tkLocs,
    outputPIDLocations=pidLocs,
    mergedOutputPIDLocation=finalPIDLoc)

# The final sequence to run
all.Members += [RichRec]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# Monitoring
from RichFutureRecMonitors.ConfiguredRecoMonitors import RichRecMonitors
RichMoni = RichRecMonitors(
    inputTrackLocations=tkLocs,
    radiators=rads,
    applyPixelClustering=pixelClustering,
    onlineBrunelMode=online,
    outputPIDLocations=pidLocs,
    histograms=histos)
# Uncomment to enable monitoring
all.Members += [RichMoni]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# UnpackMC
from Configurables import UnpackMCParticle, UnpackMCVertex
from Configurables import DataPacking__Unpack_LHCb__MCRichDigitSummaryPacker_ as RichSumUnPack
fetcher.DataKeys += ['pMC/Vertices', 'pMC/Particles']
mcUnpack = GaudiSequencer("UnpackMCSeq", MeasureTime=True)
mcUnpack.Members += [
    UnpackMCVertex(),
    UnpackMCParticle(),
    RichSumUnPack("RichSumUnPack")
]
all.Members += [mcUnpack]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# MC Checking
from RichFutureRecMonitors.ConfiguredRecoMonitors import RichRecCheckers
RichCheck = RichRecCheckers(
    inputTrackLocations=tkLocs,
    radiators=rads,
    applyPixelClustering=pixelClustering,
    onlineBrunelMode=online,
    outputPIDLocations=pidLocs,
    histograms=histos)
# Uncomment to enable checking
all.Members += [RichCheck]

# --------------------------------------------------------------------------------------
# Add RichCNN algorithm

from Configurables import Rich__Future__Rec__PixelClusterGlobalPositions as PixelClusterGlobalPositions
from Configurables import Rich__Future__Rec__PixelClusterLocalPositions as PixelClusterLocalPositions
from Configurables import Rich__CNN__CsvGenerator as CsvGenerator

global_positions = PixelClusterGlobalPositions(
    RichPixelClustersLocation = "Rec/Rich/PixelClusters/RICH1RICH2")
local_positions = PixelClusterLocalPositions()
csv_generator = CsvGenerator(
    RichPixelClustersLocation = "Rec/Rich/PixelClusters/RICH1RICH2",
    RichPixelGlobalPositionsLocation = global_positions.RichPixelGlobalPositionsLocation,
    RichPixelLocalPositionsLocation = local_positions.RichPixelLocalPositionsLocation,
    TracksLocation = "Rec/Track/BestRichLong",
    TrackLocalPointsLocation = "Rec/Rich/SegmentPositions/Local/Long",
    TrackSegmentsLocation = "Rec/Rich/TrackSegments/Long",
    TrackToSegmentsLocation = "Rec/Rich/Relations/TrackToSegments/Initial/Long",
    RichPIDsLocation = "Rec/Rich/PID/CNN/Long")

all.Members += [ global_positions, local_positions, csv_generator ]

# --------------------------------------------------------------------------------------
# Test extended MC info
#from Configurables import DataPacking__Unpack_LHCb__MCRichHitPacker_ as RichMCHitUnPack
#from Configurables import DataPacking__Unpack_LHCb__MCRichOpticalPhotonPacker_ as RichMCPhotUnPack
#fetcher.DataKeys += ['pMC/RichHits','pMC/RichOpticalPhotons']
#mcUnpack.Members += [
#    RichMCHitUnPack("RichMCHitUnPack"),
#    RichMCPhotUnPack("RichMCPhotUnPack")
#]
#from Configurables import Rich__Future__Rec__MC__Moni__XMCTests as XMCTests
#xtest = XMCTests(
#    "RichXMCTests",
#    RichPixelClustersLocation="Rec/Rich/PixelClusters/RICH1RICH2")
#all.Members += [xtest]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# ProtoParticle monitoring. For detailed PID monitoring.
protoMoniSeq = GaudiSequencer("ProtoMonitoring", MeasureTime=True)
all.Members += [protoMoniSeq]  # Uncomment to enable Proto tuples.
# Remake the protos with the new Rich PID objects
from Configurables import (
    ChargedProtoParticleAddRichInfo, ChargedProtoParticleAddCombineDLLs,
    ChargedProtoParticleMaker, DelegatingTrackSelector, TrackSelector)
makecproto = ChargedProtoParticleMaker("ProtoPMaker", AddInfo=[])
makecproto.addTool(DelegatingTrackSelector, name="TrackSelector")
tracktypes = ['Long', 'Upstream', 'Downstream', 'Ttrack']
makecproto.TrackSelector.TrackTypes = tracktypes
selector = makecproto.TrackSelector
for tsname in tracktypes:
    selector.addTool(TrackSelector, name=tsname)
    ts = getattr(selector, tsname)
    ts.TrackTypes = [tsname]
rich = addInfo(makecproto, ChargedProtoParticleAddRichInfo, "AddRich")
comb = addInfo(makecproto, ChargedProtoParticleAddCombineDLLs, "CombDLLs")
protoMoniSeq.Members += [makecproto]
# monitoring config
from Configurables import GlobalRecoChecks
GlobalRecoChecks().Sequencer = protoMoniSeq
GlobalRecoChecks().ProtoTupleName = rootFileBaseName + "-ProtoTuple.root"
GlobalRecoChecks().ANNTupleName = rootFileBaseName + "-ANNPIDTuple.root"
# ANNPID monitoring
GlobalRecoChecks().AddANNPIDInfo = False
#GlobalRecoChecks().AddANNPIDInfo = True
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# Example command lines
# --------------------------------------------------------------------------------------

# Normal running (old data format)
# gaudirun.py -T ~/LHCbCMake/Feature/Rec/Rich/RichFutureRecSys/examples/{RichFuture.py,data/PMTs/MCFormat/MCXDstFiles.py} 2>&1 | tee RichFuture-${User_release_area##/*/}-${CMTCONFIG}.log

# Normal running (realistic data format)
# gaudirun.py -T ~/LHCbCMake/Feature/Rec/Rich/RichFutureRecSys/examples/{RichFuture.py,data/PMTs/RealTel40Format/MCLDstFiles.py} 2>&1 | tee RichFuture-${User_release_area##/*/}-${CMTCONFIG}.log

# --------------------------------------------------------------------------------------
