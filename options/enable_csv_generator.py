from Gaudi.Configuration import *

def ConfGaudiSeq():
    from Configurables import Rich__CNN__CsvGenerator
    GaudiSequencer("RichCheck").Members.append(Rich__CNN__CsvGenerator())

appendPostConfigAction(ConfGaudiSeq)
