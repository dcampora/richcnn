from Gaudi.Configuration import *

def ConfGaudiSeq():
    from Configurables import Rich__CNN__ImageGenerator
    GaudiSequencer("RichCheck").Members.append(Rich__CNN__ImageGenerator())

appendPostConfigAction(ConfGaudiSeq)
