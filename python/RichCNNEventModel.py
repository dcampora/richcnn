from math import sqrt


class Pixel(object):
  def __init__(self,
    index,
    rich,
    side,
    PDCol,
    PDNumInCol,
    pixelCol,
    pixelRow,
    global_position,
    local_position,
    local_corrected_position):

    self.index = index
    self.rich = rich
    self.side = side
    self.PDCol = PDCol
    self.PDNumInCol = PDNumInCol
    self.pixelCol = pixelCol
    self.pixelRow = pixelRow
    self.global_position = global_position
    self.local_position = local_position
    self.local_corrected_position = local_corrected_position
    
  def __repr__(self):
    return "Pixel #" + str(self.index) + " rich" + str(self.rich) + \
      " " + str(self.side) + " ({:.2f}, {:.2f})".format(
        self.local_corrected_position[0], self.local_corrected_position[1])


class MCParticle(object):
  def __init__(self,
    index,
    PID,
    tracks,
    pixels):

    self.index = index
    self.PID = PID
    self.__track_ids = []
    self.__pixel_ids = []
    self.__tracks = tracks
    self.__pixels = pixels

  def _add_track(self, track_id):
    self.__track_ids.append(track_id)

  def _add_pixel(self, pixel_id):
    self.__pixel_ids.append(pixel_id)

  def pixels(self):
    return [self.__pixels[pix_id] for pix_id in self.__pixel_ids]

  def tracks(self):
    return [self.__tracks[track_id] for track_id in self.__track_ids]

  def __repr__(self):
    return "Particle #" + str(self.index) + " (" + str(self.PID) + ")"


class Track(object):
  def __init__(self,
    index,
    segments,
    track_type="Long"):

    self.index = index
    self.track_type = track_type
    self.__segment_ids = []
    self.__segments = segments

  def __repr__(self):
    return "Track #" + str(self.index)

  def _add_segment(self, segment_id):
    self.__segment_ids.append(segment_id)

  def segments(self):
    return [self.__segments[segment_id] for segment_id in self.__segment_ids]
  
  def has_segment_in_rich(self, rich=1):
    for segment in self.segments():
      if segment.rich == rich:
        return True
    return False


class Segment(object):
  def __init__(self,
    index,
    csv_index,
    track_id,
    rich,
    radiator,
    middle_point,
    middle_momentum,
    average_photon_energy,
    plane_point_0,
    plane_point_1,
    tracks,
    displacement):

    self.index = index
    self.csv_index = csv_index
    self._track_id = track_id
    self.rich = int(rich[-1])
    self.radiator = radiator
    self.middle_point = middle_point
    self.middle_momentum = middle_momentum
    self.average_photon_energy = average_photon_energy
    self.plane_point_0 = plane_point_0
    self.plane_point_1 = plane_point_1
    self.plane_average_point = self.__calculate_average_point(
      self.plane_point_0, self.plane_point_1, displacement)
    self.__tracks = tracks

  def track(self):
    return self.__tracks[self._track_id]

  def momentum(self):
    return sqrt((self.middle_momentum[0]*self.middle_momentum[0]) +
      (self.middle_momentum[1]*self.middle_momentum[1]) +
      (self.middle_momentum[2]*self.middle_momentum[2]))

  def __calculate_average_point(self, p0, p1, displacement):
    if abs(p0[0]) > 800 or abs(p0[1]) > 800:
      side = "bottom" if self.rich==1 else "right"
      return (p1[0] + displacement[self.rich][side][0], p1[1] + displacement[self.rich][side][1])
    elif abs(p1[0]) > 800 or abs(p1[1]) > 800:
      side = "top" if self.rich==1 else "left"
      return (p0[0] + displacement[self.rich][side][0], p0[1] + displacement[self.rich][side][1])
    else:
      return ((p0[0] + p1[0]) / 2, (p0[1] + p1[1]) / 2)

  def __repr__(self):
    return "Segment #" + str(self.index) + " rich" + str(self.rich)


class Event(object):
  def __init__(self,
    index,
    event_dict,
    filenames,
    debug=True,
    perform_displacement=False,
    rich_displacement={1: [-0.039, 214.43], 2: [436.91, -1.20]}):
    self.__debug = debug
    self.index = index
    self.__event_dict = event_dict
    self.__filenames = filenames
    self.__perform_displacement = perform_displacement
    self.__displacement = {
      1: {
        "top": [-rich_displacement[1][0]/2, -rich_displacement[1][1]/2],
        "bottom": [rich_displacement[1][0]/2, rich_displacement[1][1]/2]
      },
      2: {
        "left": [-rich_displacement[2][0]/2, -rich_displacement[2][1]/2],
        "right": [rich_displacement[2][0]/2, rich_displacement[2][1]/2]
      }
    }

    # Containers for all
    self.pixels = []
    self.pixel_count = []
    self.segments = []
    self.tracks = {}

    # Initialize containers from read data
    self.__initialize()

  def __repr__(self):
    return "Event #" + str(self.index) + \
      " (" + str(len(self.pixels)) + " pixels, " + \
      str(len(self.segments)) + " segments)"

  def pixels_from_panel(self, rich=1, side=""):
    begin, end = 0, 0
    # Whole rich sections
    if rich==1 and side=="":
      begin, end = 0, (self.pixel_count[0] + self.pixel_count[1])
    elif rich==2 and side=="":
      begin, end = sum(self.pixel_count[:2]), len(self.pixels)
    # Specific region
    elif rich==1 and side=="top":
      begin, end = 0, self.pixel_count[0]
    elif rich==1 and side=="bottom":
      begin, end = self.pixel_count[0], sum(self.pixel_count[:2])
    elif rich==2 and side=="left":
      begin, end = sum(self.pixel_count[:2]), sum(self.pixel_count[:3])
    elif rich==2 and side=="right":
      begin, end = sum(self.pixel_count[:3]), len(self.pixels)
    return self.pixels[begin:end]

  def pixels_from_tracks(self, rich=1, side=""):
    pixels = []
    for track in self.tracks.values():
      pixels += track.pixels_from_panel(rich=rich, side=side)
    return pixels

  def __initialize(self):
    self.__read_pixels()
    self.__read_segments()
    if self.__perform_displacement:
      self.__displace_pixels()
    self.__generate_tracks()

  def __displace_pixels(self):
    for p in self.pixels:
      p.local_corrected_position = (
        p.local_corrected_position[0] + self.__displacement[p.rich][p.side][0],
        p.local_corrected_position[1] + self.__displacement[p.rich][p.side][1]
      )

  def __generate_tracks(self):
    for segment in self.segments:
      if segment._track_id not in self.tracks:
        self.tracks[segment._track_id] = Track(segment._track_id, self.segments)
      self.tracks[segment._track_id]._add_segment(segment.index)

  def __read_segments(self):
    segment_id = 0
    dataname = self.__filenames["Segments"][0]
    for k, v in iter(self.__event_dict[dataname].items()):
      self.segments.append(Segment(segment_id,
      v["segment ID"],
      v["track ID"],
      v["RICH"],
      v["radiator"],
      (v["middle point x"], v["middle point y"], v["middle point z"]),
      (v["middle momentum x"], v["middle momentum y"], v["middle momentum z"]),
      v["average photon energy"],
      (v["topleft x"], v["topleft y"]),
      (v["bottomright x"], v["bottomright y"]),
      self.tracks,
      self.__displacement))
      segment_id += 1

  def __read_pixels(self):
    # Containers in order in which pixels are generated,
    # so that IDs coincide with their IDs
    container = [("Rich1 top", 1, "top"),
      ("Rich1 bottom", 1, "bottom"),
      ("Rich2 left", 2, "left"),
      ("Rich2 right", 2, "right")]

    for rich_name, rich, side in container:
      dataname = self.__filenames[rich_name][0]
      self.pixel_count.append(len(self.__event_dict[dataname]))

      for k, v in iter(self.__event_dict[dataname].items()):
        global_position = (v["global x"], v["global y"], v["global z"])
        local_position = (v["local x"], v["local y"])
        local_corrected_position = (v["corrected local x"], v["corrected local y"])

        self.pixels.append(Pixel(
          v["pixel ID"],
          rich,
          side,
          v["PDCol"],
          v["PDNumInCol"],
          v["pixelCol"],
          v["pixelRow"],
          global_position,
          local_position,
          local_corrected_position
        ))
