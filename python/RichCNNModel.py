import tensorflow
print("Imported tensorflow", tensorflow.__version__)

from RichCNNFileReader import RichEventReader

def CSVEventModel(model_location):
  print("Initializing CSV event model with location", model_location)
  reader = RichEventReader(containing_folder=model_location)
  event = reader.read_event()
  print("Event information:\n Pixel count:", event.pixel_count, "\n Number of tracks:", len(event.tracks), "\n Number of segments:", len(event.segments))

  def run_model_on_track(track_number):
    print("Running CNN over track number", track_number)

    if track_number in event.tracks:
      track = event.tracks[track_number]
      print("Track", track_number, ":\n has_segment_in_rich(1):", track.has_segment_in_rich(1), "\n has_segment_in_rich(2):", track.has_segment_in_rich(2))
      # Your code...

      # Expects values for:
      # Active particle: electron
      # Active particle: muon
      # Active particle: pion
      # Active particle: kaon
      # Active particle: proton
      # Active particle: deuteron
      # Active particle: below_threshold
      return [0.88, 0.5, 0.2, 0.1, 0.0, 0.7, 0.4]
    else:
      # There are no segments associated with the track,
      # therefore return below_threshold
      print("Track", track_number, " has no associated segments")
      return [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]

  return run_model_on_track
