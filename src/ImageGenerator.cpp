
// local
#include "ImageGenerator.h"
#include <utility>

using namespace Rich::CNN;

//-----------------------------------------------------------------------------
// Implementation file for class : ImageGenerator
//
// 2016-12-06 : Chris Jones
//-----------------------------------------------------------------------------

ImageGenerator::ImageGenerator( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"TrackToSegmentsLocation", "Rec/Rich/Relations/TrackToSegments/Selected/Long"},
                 KeyValue{"TrackLocalPointsLocation", "Rec/Rich/SegmentPositions/Local/Long"},
                 KeyValue{"TrackSegmentsLocation", "Rec/Rich/TrackSegments/Long"},
                 KeyValue{"RichPixelLocalPositionsLocation", "Rec/Rich/PixelPositions/Local/RICH1RICH2"},
                 KeyValue{"RichPixelClustersLocation", "Rec/Rich/PixelClusters/RICH1RICH2"},
                 KeyValue{"TracksLocation", "Rec/Track/BestRichLong"},
                 KeyValue{"TrackToMCParticlesRelations", Rich::Future::MC::Relations::TrackToMCParticles},
                 KeyValue{"RichDigitSummariesLocation", LHCb::MCRichDigitSummaryLocation::Default}} ) {
  // Write out to files
  pre       = "/afs/cern.ch/work/d/dcampora/cnn_input/";
  extension = ".bin";
  mkdir( pre.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

  // Hard-coded associations
  pid_associations = {{Rich::Electron, 0}, {Rich::Pion, 1},   {Rich::Muon, 2},
                      {Rich::Kaon, 3},     {Rich::Proton, 4}, {Rich::Deuteron, 5}};

  // Hard-coded momenta ranges and radii limits
  min_max_momenta = std::array<double, 2>( {2001, 80000} );
  momenta_ranges  = std::array<double, 144>(
      {2001,  3000,  42, 70, 90, 97, 3001,  4000,  47, 83, 91, 98, 4001,  5000,  68, 88, 92, 98,
       5001,  6000,  78, 98, -1, -1, 6001,  7000,  82, 98, -1, -1, 7001,  8000,  85, 98, -1, -1,
       8001,  9000,  86, 98, -1, -1, 9001,  10000, 87, 98, -1, -1, 10001, 11000, 42, 51, 88, 98,
       11001, 12000, 47, 60, 89, 98, 12001, 13000, 56, 67, 87, 98, 13001, 14000, 63, 72, 85, 98,
       14001, 15000, 67, 76, 90, 98, 15001, 16000, 71, 79, 89, 98, 16001, 17000, 74, 81, 90, 98,
       17001, 18000, 76, 83, 89, 98, 18001, 19000, 78, 85, 91, 98, 19001, 20000, 79, 86, 91, 98,
       20001, 30000, 50, 77, 88, 98, 30001, 40000, 75, 98, -1, -1, 40001, 50000, 83, 98, -1, -1,
       50001, 60000, 87, 99, -1, -1, 60001, 70000, 89, 98, -1, -1, 70001, 80000, 90, 98, -1, -1} );
}

StatusCode ImageGenerator::initialize() {
  // Sets up various tools and services
  auto sc = Consumer::initialize();
  if ( !sc ) return sc;

  // Open all files in the momenta ranges
  // and initialize counter
  for ( size_t i = 0; i < momenta_ranges.size() / 6; ++i ) {
    std::array<double, 6> mrange{momenta_ranges[6 * i],     momenta_ranges[6 * i + 1], momenta_ranges[6 * i + 2],
                                 momenta_ranges[6 * i + 3], momenta_ranges[6 * i + 4], momenta_ranges[6 * i + 5]};

    std::string containing_folder =
        pre + std::to_string( (unsigned)mrange[0] ) + "_" + std::to_string( (unsigned)mrange[1] ) + "/";
    mkdir( containing_folder.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

    files[mrange[0]]["containing_folder"]    = containing_folder;
    files[mrange[0]]["current_file_counter"] = "0";
    files[mrange[0]]["current_file_index"] =
        std::to_string( Common::first_free_index( files[mrange[0]]["containing_folder"] + "x_" ) );
    files[mrange[0]]["current_filename"] = files[mrange[0]]["current_file_index"] + ".bin";

    x_descriptors[mrange[0]] =
        get_ofstream( files[mrange[0]]["containing_folder"] + "x_" + files[mrange[0]]["current_filename"] );
    n_descriptors[mrange[0]] =
        get_ofstream( files[mrange[0]]["containing_folder"] + "n_" + files[mrange[0]]["current_filename"] );
    p_descriptors[mrange[0]] =
        get_ofstream( files[mrange[0]]["containing_folder"] + "p_" + files[mrange[0]]["current_filename"] );
    y_descriptors[mrange[0]] =
        get_ofstream( files[mrange[0]]["containing_folder"] + "y_" + files[mrange[0]]["current_filename"] );
  }
  return sc;
}

StatusCode ImageGenerator::finalize() {
  // Close all open files
  for ( size_t i = 0; i < momenta_ranges.size() / 6; ++i ) {
    std::array<double, 6> mrange{momenta_ranges[6 * i],     momenta_ranges[6 * i + 1], momenta_ranges[6 * i + 2],
                                 momenta_ranges[6 * i + 3], momenta_ranges[6 * i + 4], momenta_ranges[6 * i + 5]};
    x_descriptors[mrange[0]].close();
    n_descriptors[mrange[0]].close();
    p_descriptors[mrange[0]].close();
    y_descriptors[mrange[0]].close();
  }
  return Consumer::finalize();
}

// Gets a descriptor from the requested momentum range
// Checks if that descriptor is done and in that case, opens a new one
void ImageGenerator::increment_file_counter( const std::array<double, 6>& mrange ) const {
  auto& file    = files[mrange[0]];
  int   counter = ( std::stoi( file["current_file_counter"] ) ) + 1;
  if ( counter > max_elements_per_file ) {
    // Close file, open new one
    x_descriptors[mrange[0]].close();
    n_descriptors[mrange[0]].close();
    p_descriptors[mrange[0]].close();
    y_descriptors[mrange[0]].close();
    file["current_file_index"]   = std::to_string( ( std::stoi( file["current_file_index"] ) ) + 1 );
    file["current_file_counter"] = "1";
    file["current_filename"]     = file["current_file_index"] + ".bin";
    x_descriptors[mrange[0]]     = get_ofstream( file["containing_folder"] + "x_" + file["current_filename"] );
    n_descriptors[mrange[0]]     = get_ofstream( file["containing_folder"] + "n_" + file["current_filename"] );
    p_descriptors[mrange[0]]     = get_ofstream( file["containing_folder"] + "p_" + file["current_filename"] );
    y_descriptors[mrange[0]]     = get_ofstream( file["containing_folder"] + "y_" + file["current_filename"] );
  } else {
    file["current_file_counter"] = std::to_string( counter );
  }
}

// Helper function to get the ofstream
std::ofstream ImageGenerator::get_ofstream( const std::string& filename ) const {
  std::ofstream ofs;
  ofs.open( filename, std::ofstream::out | std::ofstream::binary );
  return ofs;
}

/**
 * @brief      Finds the Monte Carlo particle MCP.
 *             If it exists already, returns the key.
 *             If it doesn't, add it, create an empty set of clusters for it,
 *             and return the key.
 */
size_t ImageGenerator::fetch_key( std::vector<const LHCb::MCParticle*>& MCPs,
                                  std::vector<std::vector<size_t>>& mcp_clusters, const LHCb::MCParticle* MCP ) const {
  auto f = std::find( MCPs.begin(), MCPs.end(), MCP );
  if ( f != MCPs.end() ) {
    return std::distance( MCPs.begin(), f );
  } else {
    MCPs.push_back( MCP );
    mcp_clusters.push_back( {} );
    return ( MCPs.size() - 1 );
  }
}

/**
 * @brief Prints an image bits
 */
void ImageGenerator::print_image( const std::array<uint64_t, 64>& image_pixels ) const {
  for ( size_t i = 0; i < image_pixels.size(); ++i ) {
    uint64_t row_pixels = image_pixels[i];

    for ( size_t j = 0; j < image_pixels.size(); ++j ) {
      std::cout << ( ( row_pixels >> ( image_pixels.size() - 1 - j ) ) & 0x01 );
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
}

void ImageGenerator::operator()( const Rich::Future::Rec::Relations::TrackToSegments::Vector& tkToSegRels,
                                 const SegmentPanelSpacePoints::Vector&                       trHitPntsLoc,
                                 const LHCb::RichTrackSegment::Vector& segments, const SpacePointVector& localPoints,
                                 const Rich::PDPixelCluster::Vector& clusters, const LHCb::Track::Selection& tracks,
                                 const Rich::Future::MC::Relations::TkToMCPRels& tkrels,
                                 const LHCb::MCRichDigitSummarys&                digitSums ) const {
  // Fetch Monte Carlo helper
  Rich::Future::Rec::MC::Helper mcHelper( tkrels, digitSums );

  // Generate the pixel vector with all displaced pixels
  std::vector<std::array<double, 2>> displacedPixels = Common::get_displaced_pixels( localPoints, clusters );

  // For each Monte Carlo particle, fetch all cluster indices
  std::vector<const LHCb::MCParticle*> MCPs;
  std::vector<std::vector<size_t>>     mcp_clusters;
  for ( size_t i = 0; i < clusters.size(); ++i ) {
    auto& cluster = clusters[i];
    // Get MCParticles for the cluster
    const auto mcParts = mcHelper.mcParticles( cluster );
    // Loop over all MCParticles associated to the pixel
    for ( const auto* MCP : mcParts ) {
      int key = fetch_key( MCPs, mcp_clusters, MCP );
      mcp_clusters[key].push_back( i );
    }
  }

  // Generate MCP and noise in different datasets
  if ( generate_separate_mcp_noise ) {
    // Iterate over all segments
    for ( const auto& inTkRel : tkToSegRels ) {
      // Fetch for each track the first associated particle
      // Get the MCParticles for this track
      const auto associated_mcps = mcHelper.mcParticles( *( tracks[inTkRel.tkIndex] ), true, 0.5 );
      if ( !associated_mcps.empty() ) {
        for ( auto& associated_mcp : associated_mcps ) {
          Rich::ParticleIDType p = mcHelper.mcParticleType( associated_mcp );
          if ( pid_associations.find( p ) != pid_associations.end() ) {
            uint8_t pid = 1 << pid_associations[p];

            // Fetch Monte Carlo particle key and mcp clusters index
            int key = fetch_key( MCPs, mcp_clusters, associated_mcp );

            // Generate Monte Carlo image
            // Do it only if there is at least one pixel (truly) associated with this particle
            if ( !mcp_clusters[key].empty() ) {
              for ( const auto& segIndex : inTkRel.segmentIndices ) {
                auto segment = segments[segIndex];
                if ( segment.rich() == Rich::Rich1 ) {
                  // Rich::RichTrackSegment
                  const auto& momentum = floor( segment.bestMomentumMag() );
                  auto        tkLocPtn = trHitPntsLoc[segIndex];
                  const auto& p0       = tkLocPtn.point( Rich::top );
                  const auto& p1       = tkLocPtn.point( Rich::bottom );

                  if ( momentum >= min_max_momenta[0] && momentum <= min_max_momenta[1] ) {
                    // Initialize image to 1s
                    std::array<uint64_t, 64> image_pixels;
                    std::array<uint64_t, 64> noise_image_pixels;
                    for ( auto& bits : image_pixels ) { bits = ( (uint64_t)0xFFFFFFFFFFFFFFFF ); }
                    for ( auto& bits : noise_image_pixels ) { bits = ( (uint64_t)0xFFFFFFFFFFFFFFFF ); }

                    // Get range we are on
                    const auto range = Common::get_range( momentum, momenta_ranges );

                    // Calculate middle point of segment
                    std::array<double, 2> middle_point = {( p0.x() + p1.x() ) / 2.0, ( p0.y() + p1.y() ) / 2.0};
                    if ( fabs( p0.x() ) > 800 or fabs( p0.y() ) > 800 ) {
                      // Bottom
                      middle_point[0] = p1.x() + Common::rich_displacements[Rich::bottom][0];
                      middle_point[1] = p1.y() + Common::rich_displacements[Rich::bottom][1];
                    } else if ( fabs( p1.x() > 800 or fabs( p1.y() ) > 800 ) ) {
                      // Top
                      middle_point[0] = p0.x() + Common::rich_displacements[Rich::top][0];
                      middle_point[1] = p0.y() + Common::rich_displacements[Rich::top][1];
                    }

                    // Calculate all Monte Carlo pixels within the radius (theta, radius)
                    std::vector<std::array<int, 2>> pixelsWithinRadius;
                    for ( auto i : mcp_clusters[key] ) {
                      const auto& cluster = clusters[i];
                      auto        smartID = cluster.primaryID();
                      // Only pixels in Rich1
                      if ( smartID.rich() == Rich::Rich1 ) {
                        auto& pixel = displacedPixels[i];
                        auto  rho   = Common::euclidean_distance( middle_point, pixel );
                        if ( ( rho >= range[2] && rho <= range[3] ) || ( rho >= range[4] && rho <= range[5] ) ) {
                          // Make a single range out of the two ranges
                          const auto second_range_length = range[5] - range[4];
                          const auto range_start         = range[2];
                          const auto range_end           = range[3] + second_range_length;
                          if ( second_range_length > 0 && rho >= range[4] && rho <= range[5] ) {
                            rho -= ( range[4] - range[3] );
                          }

                          const auto theta = Common::angle( middle_point, pixel );

                          // std::cout << theta << ", " << rho
                          //   << ", (" << Common::normalize(theta, -M_PI, M_PI, 0, image_size - 1)
                          //   << ", " << Common::normalize(rho, range_start, range_end, 0, image_size - 1)
                          //   << ")" << std::endl;

                          pixelsWithinRadius.push_back(
                              {Common::normalize( theta, -M_PI, M_PI, 0, image_size - 1 ),
                               Common::normalize( rho, range_start, range_end, 0, image_size - 1 )} );
                        }
                      }
                    }

                    // Add all pixels to the bits
                    for ( auto pixel : pixelsWithinRadius ) {
                      image_pixels[pixel[0]] &= ~( ( (uint64_t)0x0000000000000000 ) | ( ( (uint64_t)1 ) << pixel[1] ) );
                    }

                    // Calculate all noise pixels within radius
                    std::vector<std::array<int, 2>> noisePixelsWithinRadius;
                    for ( size_t i = 0; i < clusters.size(); ++i ) {
                      if ( std::find( mcp_clusters[key].begin(), mcp_clusters[key].end(), i ) ==
                           std::end( mcp_clusters[key] ) ) {
                        const auto& cluster = clusters[i];
                        auto        smartID = cluster.primaryID();
                        // Only pixels in Rich1
                        if ( smartID.rich() == Rich::Rich1 ) {
                          auto& pixel = displacedPixels[i];
                          auto  rho   = Common::euclidean_distance( middle_point, pixel );
                          if ( ( rho >= range[2] && rho <= range[3] ) || ( rho >= range[4] && rho <= range[5] ) ) {
                            // Make a single range out of the two ranges
                            const auto second_range_length = range[5] - range[4];
                            const auto range_start         = range[2];
                            const auto range_end           = range[3] + second_range_length;
                            if ( second_range_length > 0 && rho >= range[4] && rho <= range[5] ) {
                              rho -= ( range[4] - range[3] );
                            }

                            const auto theta = Common::angle( middle_point, pixel );
                            noisePixelsWithinRadius.push_back(
                                {Common::normalize( theta, -M_PI, M_PI, 0, image_size - 1 ),
                                 Common::normalize( rho, range_start, range_end, 0, image_size - 1 )} );
                          }
                        }
                      }
                    }

                    // Add all noise pixels to the bits
                    for ( auto pixel : noisePixelsWithinRadius ) {
                      noise_image_pixels[pixel[0]] &=
                          ~( ( (uint64_t)0x0000000000000000 ) | ( ( (uint64_t)1 ) << pixel[1] ) );
                    }

                    // Append this segment's information to the open file
                    increment_file_counter( range );
                    auto& x_descriptor = x_descriptors[range[0]];
                    auto& n_descriptor = n_descriptors[range[0]];
                    auto& p_descriptor = p_descriptors[range[0]];
                    auto& y_descriptor = y_descriptors[range[0]];
                    for ( auto& pixel_row : image_pixels ) {
                      x_descriptor.write( (char*)&pixel_row, sizeof( uint64_t ) );
                    }
                    for ( auto& pixel_row : noise_image_pixels ) {
                      n_descriptor.write( (char*)&pixel_row, sizeof( uint64_t ) );
                    }
                    p_descriptor.write( (char*)&momentum, sizeof( float ) );
                    y_descriptor.write( (char*)&pid, sizeof( uint8_t ) );
                  }
                }
              }
            }
          }
        }
      }
    }
  } else {
    // Iterate over all segments
    for ( const auto& inTkRel : tkToSegRels ) {
      // Fetch for each track the first associated particle
      // Get the MCParticles for this track
      const auto associated_mcps = mcHelper.mcParticles( *( tracks[inTkRel.tkIndex] ), true, 0.5 );
      if ( !associated_mcps.empty() ) {
        for ( auto& associated_mcp : associated_mcps ) {
          Rich::ParticleIDType p = mcHelper.mcParticleType( associated_mcp );
          if ( pid_associations.find( p ) != pid_associations.end() ) {
            uint8_t pid = 1 << pid_associations[p];

            // Fetch Monte Carlo particle key and mcp clusters index
            int key = fetch_key( MCPs, mcp_clusters, associated_mcp );

            // Generate image
            // Do it only if there is at least one pixel (truly) associated with this particle
            if ( !mcp_clusters[key].empty() ) {
              for ( const auto& segIndex : inTkRel.segmentIndices ) {
                auto segment = segments[segIndex];
                if ( segment.rich() == Rich::Rich1 ) {
                  // Rich::RichTrackSegment
                  const auto& momentum = floor( segment.bestMomentumMag() );
                  auto        tkLocPtn = trHitPntsLoc[segIndex];
                  const auto& p0       = tkLocPtn.point( Rich::top );
                  const auto& p1       = tkLocPtn.point( Rich::bottom );

                  // Initialize image to 1s
                  std::array<uint64_t, 64> image_pixels;
                  for ( auto& bits : image_pixels ) { bits = ( (uint64_t)0xFFFFFFFFFFFFFFFF ); }

                  if ( momentum >= min_max_momenta[0] && momentum <= min_max_momenta[1] ) {
                    // Get range we are on
                    const auto range = Common::get_range( momentum, momenta_ranges );

                    // Calculate middle point of segment
                    std::array<double, 2> middle_point = {( p0.x() + p1.x() ) / 2.0, ( p0.y() + p1.y() ) / 2.0};
                    if ( fabs( p0.x() ) > 800 or fabs( p0.y() ) > 800 ) {
                      // Bottom
                      middle_point[0] = p1.x() + Common::rich_displacements[Rich::bottom][0];
                      middle_point[1] = p1.y() + Common::rich_displacements[Rich::bottom][1];
                    } else if ( fabs( p1.x() > 800 or fabs( p1.y() ) > 800 ) ) {
                      // Top
                      middle_point[0] = p0.x() + Common::rich_displacements[Rich::top][0];
                      middle_point[1] = p0.y() + Common::rich_displacements[Rich::top][1];
                    }

                    // Calculate all pixels within the radius (theta, radius)
                    std::vector<std::array<int, 2>> pixelsWithinRadius;
                    for ( size_t i = 0; i < clusters.size(); ++i ) {
                      const auto& cluster = clusters[i];
                      auto        smartID = cluster.primaryID();
                      // Only pixels in Rich1
                      if ( smartID.rich() == Rich::Rich1 ) {
                        auto& pixel = displacedPixels[i];
                        auto  rho   = Common::euclidean_distance( middle_point, pixel );
                        if ( ( rho >= range[2] && rho <= range[3] ) || ( rho >= range[4] && rho <= range[5] ) ) {
                          // Make a single range out of the two ranges
                          const auto second_range_length = range[5] - range[4];
                          const auto range_start         = range[2];
                          const auto range_end           = range[3] + second_range_length;
                          if ( second_range_length > 0 && rho >= range[4] && rho <= range[5] ) {
                            rho -= ( range[4] - range[3] );
                          }

                          const auto theta = Common::angle( middle_point, pixel );
                          pixelsWithinRadius.push_back(
                              {Common::normalize( theta, -M_PI, M_PI, 0, image_size - 1 ),
                               Common::normalize( rho, range_start, range_end, 0, image_size - 1 )} );
                        }
                      }
                    }

                    // Add all pixels to the bits
                    for ( auto pixel : pixelsWithinRadius ) {
                      image_pixels[pixel[0]] &= ~( ( (uint64_t)0x0000000000000000 ) | ( ( (uint64_t)1 ) << pixel[1] ) );
                    }

                    // Append this segment's information to the open file
                    increment_file_counter( range );
                    auto& x_descriptor = x_descriptors[range[0]];
                    auto& p_descriptor = p_descriptors[range[0]];
                    auto& y_descriptor = y_descriptors[range[0]];
                    for ( auto& pixel_row : image_pixels ) {
                      x_descriptor.write( (char*)&pixel_row, sizeof( uint64_t ) );
                    }
                    p_descriptor.write( (char*)&momentum, sizeof( float ) );
                    y_descriptor.write( (char*)&pid, sizeof( uint8_t ) );
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( ImageGenerator )

//-----------------------------------------------------------------------------
