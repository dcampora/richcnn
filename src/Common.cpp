#include "Common.h"

using namespace Rich::CNN;

// Initialization of static data members
Rich::Rec::RadPositionCorrector<double> Common::corrector;

std::map<Rich::DetectorType, std::array<double, 2>> Common::displacements =
    std::map<Rich::DetectorType, std::array<double, 2>>(
        {{Rich::Rich1, {0.00353256, 216.923}}, {Rich::Rich2, {-77.3564, -6.22386}}} );

std::map<int8_t, std::array<double, 2>> Common::rich_displacements = std::map<int8_t, std::array<double, 2>>( {
    {Rich::top, {-Common::displacements[Rich::Rich1][0] / 2, -Common::displacements[Rich::Rich1][1] / 2}},
    {Rich::bottom, {Common::displacements[Rich::Rich1][0] / 2, Common::displacements[Rich::Rich1][1] / 2}},
    {Rich::left, {-Common::displacements[Rich::Rich2][0] / 2, -Common::displacements[Rich::Rich2][1] / 2}},
    {Rich::right, {Common::displacements[Rich::Rich2][0] / 2, Common::displacements[Rich::Rich2][1] / 2}},
} );

std::map<Rich::DetectorType, Rich::RadiatorType> Common::gas = {{Rich::Rich1, Rich::Rich1Gas},
                                                                {Rich::Rich2, Rich::Rich2Gas}};
