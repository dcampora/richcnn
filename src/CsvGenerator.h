
#pragma once

// STD
#include <algorithm>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Event model
#include "Event/MCRichDigitSummary.h"

// Relations
#include "RichFutureMCUtils/RichRecMCHelper.h"

#include "RichFutureRecEvent/RichRecSpacePoints.h"
#include "RichInterfaces/IRichSmartIDTool.h"
#include <fstream>
#include <sys/stat.h>

// Relations
#include "RichFutureMCUtils/RichRecMCHelper.h"
#include "RichRecUtils/RichRadCorrLocalPositions.h"

#include <array>
#include <bitset>
#include <map>
#include <vector>

#include "Common.h"

// Use the functional framework
using namespace Gaudi::Functional;
using namespace Rich::Future::Rec;

namespace Rich::CNN {

  class CsvGenerator final : public Consumer<void( const Rich::Future::Rec::Relations::TrackToSegments::Vector&, //
                                                   const SegmentPanelSpacePoints::Vector&,                       //
                                                   const LHCb::RichTrackSegment::Vector&,                        //
                                                   const SpacePointVector&,                                      //
                                                   const SpacePointVector&,                                      //
                                                   const Rich::PDPixelCluster::Vector&,                          //
                                                   const LHCb::Track::Selection&,                                //
                                                   const Rich::Future::MC::Relations::TkToMCPRels&,              //
                                                   const LHCb::MCRichDigitSummarys& )> {
  private:
    std::string                                   pre, extension;
    mutable int                                   current_folder;
    mutable std::map<std::string, std::string>    preface;
    mutable std::map<std::string, std::ofstream*> ofs;

    void           open_ofstreams() const;
    void           close_ofstreams() const;
    void           write_preface() const;
    std::ofstream* rich_ofstream( const Rich::DetectorType& type, const Rich::Side& side ) const;
    size_t         fetch_key( std::vector<const LHCb::MCParticle*>& MCPs, const LHCb::MCParticle* MCP ) const;

  public:
    /// Standard constructor
    CsvGenerator( const std::string& name, ISvcLocator* pSvcLocator );

    /// Functional operator
    void operator()( const Rich::Future::Rec::Relations::TrackToSegments::Vector&, //
                     const SegmentPanelSpacePoints::Vector&,                       //
                     const LHCb::RichTrackSegment::Vector&,                        //
                     const SpacePointVector&,                                      //
                     const SpacePointVector&,                                      //
                     const Rich::PDPixelCluster::Vector&,                          //
                     const LHCb::Track::Selection&,                                //
                     const Rich::Future::MC::Relations::TkToMCPRels&,              //
                     const LHCb::MCRichDigitSummarys& ) const override;
  };

} // namespace Rich::CNN
