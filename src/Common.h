
#pragma once

// STD
#include <algorithm>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Event model
#include "Event/MCRichDigitSummary.h"

// Relations
#include "RichFutureMCUtils/RichRecMCHelper.h"

#include "RichFutureRecEvent/RichRecSpacePoints.h"
#include "RichInterfaces/IRichSmartIDTool.h"
#include <fstream>
#include <sys/stat.h>

// Relations
#include "RichFutureMCUtils/RichRecMCHelper.h"
#include "RichRecUtils/RichRadCorrLocalPositions.h"

#include <array>
#include <bitset>
#include <map>
#include <vector>

// Use the functional framework
using namespace Gaudi::Functional;
using namespace Rich::Future::Rec;

namespace Rich::CNN {

  class Common final {
  public:
    static Rich::Rec::RadPositionCorrector<double>             corrector;
    static std::map<Rich::DetectorType, std::array<double, 2>> displacements;
    static std::map<int8_t, std::array<double, 2>>             rich_displacements;
    static std::map<Rich::DetectorType, Rich::RadiatorType>    gas;

    static std::vector<std::array<double, 2>> get_displaced_pixels( const SpacePointVector&             localPoints,
                                                                    const Rich::PDPixelCluster::Vector& clusters ) {
      std::vector<std::array<double, 2>> displacedPixels;
      for ( unsigned i = 0; i < localPoints.size(); ++i ) {
        const auto& locPos  = localPoints[i];
        auto        smartID = clusters[i].primaryID();
        // We need the corrected and displaced pixel
        const auto            correctedPix = Common::corrector.correct( locPos, Common::gas[smartID.rich()] );
        std::array<double, 2> displacedPix = {correctedPix.x() + Common::rich_displacements[smartID.panel()][0],
                                              correctedPix.y() + Common::rich_displacements[smartID.panel()][1]};
        displacedPixels.push_back( displacedPix );
      }
      return displacedPixels;
    }

    /// Fetches the first free index on the folder of the form prefix+<number>.bin
    static int first_free_index( const std::string& prefix, const std::string& extension = ".bin" ) {
      struct stat buffer;
      unsigned    i = 0;
      while ( stat( ( prefix + std::to_string( i ) + extension ).c_str(), &buffer ) == 0 ) { ++i; }
      return i;
    }

    /// Facilitate dealing with momenta ranges
    template <size_t N>
    static std::array<double, 6> get_range( const double momentum, const std::array<double, N>& momenta_ranges ) {
      if ( momentum < momenta_ranges[0] ) {
        return {momenta_ranges[0], momenta_ranges[1], momenta_ranges[2],
                momenta_ranges[3], momenta_ranges[4], momenta_ranges[5]};
      } else if ( momentum > momenta_ranges[momenta_ranges.size() - 5] ) {
        const auto idx = momenta_ranges.size() - 6;
        return {momenta_ranges[idx],     momenta_ranges[idx + 1], momenta_ranges[idx + 2],
                momenta_ranges[idx + 3], momenta_ranges[idx + 4], momenta_ranges[idx + 5]};
      } else {
        std::array<double, 6> range = {0, 0, 0, 0};
        for ( size_t i = 0; i < momenta_ranges.size() / 6; ++i ) {
          if ( momentum >= momenta_ranges[6 * i] && momentum <= momenta_ranges[6 * i + 1] ) {
            range[0] = momenta_ranges[6 * i];
            range[1] = momenta_ranges[6 * i + 1];
            range[2] = momenta_ranges[6 * i + 2];
            range[3] = momenta_ranges[6 * i + 3];
            range[4] = momenta_ranges[6 * i + 4];
            range[5] = momenta_ranges[6 * i + 5];
            return range;
          }
        }
        return range;
      }
    }

    static double euclidean_distance( const std::array<double, 2>& p0, const std::array<double, 2>& p1 ) {
      const auto _x = p0[0] - p1[0];
      const auto _y = p0[1] - p1[1];
      return sqrt( _x * _x + _y * _y );
    }

    static double angle( const std::array<double, 2>& p0, const std::array<double, 2>& p1 ) {
      const auto _x = p1[0] - p0[0];
      const auto _y = p1[1] - p0[1];
      return atan2( _y, _x );
    }

    static int normalize( const double value, const double range_start, const double range_end,
                          const double requested_start, const double requested_end ) {
      const auto _value     = value - range_start;
      const auto _range_end = range_end - range_start;
      const auto v0         = _value / _range_end;
      const auto ymax       = requested_end - requested_start + 1;
      const auto v_in_range = floor( v0 * ymax );
      if ( v_in_range == ymax ) { return ymax - 1; }
      return v_in_range;
    }
  };

} // namespace Rich::CNN
