
#pragma once

// STD
#include <algorithm>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Event model
#include "Event/RichPID.h"

// Relations
#include "RichFutureRecEvent/RichRecSpacePoints.h"
#include "RichInterfaces/IRichSmartIDTool.h"
#include <fstream>
#include <sys/stat.h>

// Relations
#include "RichRecUtils/RichRadCorrLocalPositions.h"

#include <array>
#include <bitset>
#include <map>
#include <vector>

#include "Common.h"
#include "Python.h"
#include "RichFutureRecBase/RichRecBase.h"

// Use the functional framework
using namespace Gaudi::Functional;
using namespace Rich::Future::Rec;

namespace Rich::CNN {

  class RichCNNSolver final
      : public Transformer<LHCb::RichPIDs( const Rich::Future::Rec::Relations::TrackToSegments::Vector&, //
                                           const SegmentPanelSpacePoints::Vector&,                       //
                                           const LHCb::RichTrackSegment::Vector&,                        //
                                           const SpacePointVector&,                                      //
                                           const SpacePointVector&,                                      //
                                           const Rich::PDPixelCluster::Vector&,                          //
                                           const LHCb::Track::Selection&,
                                           const Summary::Track::Vector&),
                           Traits::BaseClass_t<AlgBase<>>> {
  private:
    std::string m_rich_cnn_temp_location = "/tmp/richcnn/";
    std::string m_extension              = ".csv";
    std::string m_preface_rich = "pixel ID,PDCol,PDNumInCol,pixelCol,pixelRow,global x,global y,global z,local x,local "
                                 "y,local z,corrected local x,corrected local y";
    std::map<std::string, std::string> m_preface = std::map<std::string, std::string>(
        { { "rich1_top", m_preface_rich },
          { "rich1_bottom", m_preface_rich },
          { "rich2_left", m_preface_rich },
          { "rich2_right", m_preface_rich },
          { "segments",
            "segment ID,track ID,RICH,radiator,middle point x,middle point y,middle point z,middle momentum x,\
middle momentum y,middle momentum z,average photon energy,topleft x,topleft y,topleft z,bottomright x,bottomright y,bottomright z,\
entry point x,entry point y,entry point z,exit point x,exit point y,exit point z" } } );

    std::map<std::string, std::ofstream*> open_ofstreams() const;
    void                                  close_ofstreams( std::map<std::string, std::ofstream*>& ofs ) const;
    void                                  write_preface( std::map<std::string, std::ofstream*>& ofs ) const;
    std::ofstream*                        rich_ofstream( const Rich::DetectorType& type, const Rich::Side& side,
                                                         std::map<std::string, std::ofstream*>& ofs ) const;

  public:
    /// Standard constructor
    RichCNNSolver( const std::string& name, ISvcLocator* pSvcLocator );

    /// Functional operator
    LHCb::RichPIDs operator()( const Rich::Future::Rec::Relations::TrackToSegments::Vector&, //
                               const SegmentPanelSpacePoints::Vector&,                       //
                               const LHCb::RichTrackSegment::Vector&,                        //
                               const SpacePointVector&,                                      //
                               const SpacePointVector&,                                      //
                               const Rich::PDPixelCluster::Vector&, const LHCb::Track::Selection&,
                               const Summary::Track::Vector&) const override;

    PyObject* fetch_model( const std::string& csv_event_location ) const;

    std::vector<float> predict_track( const LHCb::Track* track, PyObject* model ) const;
  };

} // namespace Rich::CNN
