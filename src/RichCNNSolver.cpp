
// local
#include "RichCNNSolver.h"
#include <cassert>
#include <fenv.h>
#include <utility>

// Rich Utils
#include "RichUtils/ZipRange.h"

using namespace Rich::CNN;

namespace {
  /// Helper class to protect calls to Python code with the GIL.
  struct PyGILGuard {
    PyGILGuard() : gstate( PyGILState_Ensure() ) {}
    ~PyGILGuard() { PyGILState_Release( gstate ); }
    PyGILState_STATE gstate;
  };
} // namespace

//-----------------------------------------------------------------------------
// Implementation file for class : RichCNNSolver
//
// 2016-12-06 : Chris Jones
// 2021-04-07 : Daniel Campora
//-----------------------------------------------------------------------------

RichCNNSolver::RichCNNSolver( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer(
          name, pSvcLocator,
          // input
          { KeyValue{ "TrackToSegmentsLocation", Rich::Future::Rec::Relations::TrackToSegmentsLocation::Selected },
            KeyValue{ "TrackLocalPointsLocation", SpacePointLocation::SegmentsLocal },
            KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
            KeyValue{ "RichPixelGlobalPositionsLocation", SpacePointLocation::PixelsGlobal },
            KeyValue{ "RichPixelLocalPositionsLocation", SpacePointLocation::PixelsLocal },
            KeyValue{ "RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default },
            KeyValue{ "TracksLocation", LHCb::TrackLocation::Default },
            KeyValue{ "SummaryTracksLocation", Summary::TESLocations::Tracks} },
          // output
          { KeyValue{ "RichPIDsLocation", LHCb::RichPIDLocation::Default } } ) {
  // Make temp dir
  mkdir( ( m_rich_cnn_temp_location ).c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
}

std::map<std::string, std::ofstream*> RichCNNSolver::open_ofstreams() const {
  std::map<std::string, std::ofstream*> ofs;
  for ( auto& item : m_preface ) {
    ofs[item.first] = new std::ofstream( m_rich_cnn_temp_location + item.first + m_extension );
  }
  return ofs;
}

void RichCNNSolver::close_ofstreams( std::map<std::string, std::ofstream*>& ofs ) const {
  for ( auto& item : ofs ) {
    item.second->close();
    // Ask the internet
    delete item.second;
    item.second = 0;
  }
}

void RichCNNSolver::write_preface( std::map<std::string, std::ofstream*>& ofs ) const {
  for ( auto& item : ofs ) { *( item.second ) << m_preface.at( item.first ) << std::endl; }
}

std::ofstream* RichCNNSolver::rich_ofstream( const Rich::DetectorType& type, const Rich::Side& side,
                                             std::map<std::string, std::ofstream*>& ofs ) const {
  if ( type == Rich::Rich1 ) {
    if ( side == Rich::top ) {
      return ofs["rich1_top"];
    } else {
      return ofs["rich1_bottom"];
    }
  } else {
    if ( side == Rich::left ) {
      return ofs["rich2_left"];
    } else {
      return ofs["rich2_right"];
    }
  }
}

LHCb::RichPIDs RichCNNSolver::operator()( const Rich::Future::Rec::Relations::TrackToSegments::Vector& tkToSegRels,
                                          const SegmentPanelSpacePoints::Vector&                       trHitPntsLoc,
                                          const LHCb::RichTrackSegment::Vector&                        segments,
                                          const SpacePointVector& globalPoints, const SpacePointVector& localPoints,
                                          const Rich::PDPixelCluster::Vector& clusters,
                                          const LHCb::Track::Selection&       tracks,
                                          const Summary::Track::Vector& gtracks) const {

  // Open new files for this event
  auto ofs = open_ofstreams();
  write_preface( ofs );

  // Generate the pixel vector with all displaced pixels
  std::vector<std::array<double, 2>> displacedPixels = Common::get_displaced_pixels( localPoints, clusters );

  // Iterate over all segments
  for ( const auto& inTkRel : tkToSegRels ) {
    for ( const auto& segIndex : inTkRel.segmentIndices ) {
      // Rich::RichTrackSegment
      const auto& segment         = segments[segIndex];
      const auto& middlePoint     = segment.middlePoint();
      const auto& middleMomentum  = segment.middleMomentum();
      const auto& avPhotonEnergy  = segment.avPhotonEnergy();
      const auto& tkLocPtn        = trHitPntsLoc[segIndex];
      const auto& segPanelPntZero = tkLocPtn.point( Rich::top );
      const auto& segPanelPntOne  = tkLocPtn.point( Rich::bottom );
      const auto  entryPoint      = segment.entryPoint();
      const auto  exitPoint       = segment.exitPoint();

      *ofs["segments"] << segIndex << "," << inTkRel.tkIndex << "," << segment.rich() << "," << segment.radiator()
                       << "," << middlePoint.x() << "," << middlePoint.y() << "," << middlePoint.z() << ","
                       << middleMomentum.x() << "," << middleMomentum.y() << "," << middleMomentum.z() << ","
                       << avPhotonEnergy << "," << segPanelPntZero.x() << "," << segPanelPntZero.y() << ","
                       << segPanelPntZero.z() << "," << segPanelPntOne.x() << "," << segPanelPntOne.y() << ","
                       << segPanelPntOne.z() << "," << entryPoint.x() << "," << entryPoint.y() << "," << entryPoint.z()
                       << "," << exitPoint.x() << "," << exitPoint.y() << "," << exitPoint.z() << std::endl;
    }
  }

  for ( size_t i = 0; i < clusters.size(); ++i ) {
    const auto& cluster        = clusters[i];
    const auto& gloPos         = globalPoints[i];
    const auto& locPos         = localPoints[i];
    const auto& displacedPixel = displacedPixels[i];

    auto smartID    = cluster.primaryID();
    auto ofs_output = rich_ofstream( smartID.rich(), smartID.panel(), ofs );

    *ofs_output << i
                << ","
                // PD
                << smartID.pdCol() << "," << smartID.pdNumInCol()
                << ","
                // Pixel
                << smartID.pixelCol() << "," << smartID.pixelRow()
                << ","
                // Global, local and local corrected positions
                << gloPos.x() << "," << gloPos.y() << "," << gloPos.z() << "," << locPos.x() << "," << locPos.y() << ","
                << locPos.z() << "," << displacedPixel[0] << "," << displacedPixel[1] << std::endl;
  }

  // Close files
  close_ofstreams( ofs );

  // Prepare environment to invoke Python
  const auto fpeflags = fegetexcept();
  fedisableexcept( FE_ALL_EXCEPT );
  PyGILGuard gil;

  // Fetch model
  auto rich_cnn_model = fetch_model( m_rich_cnn_temp_location );

  // Create PID container
  LHCb::RichPIDs rPIDs;
  rPIDs.reserve( tracks.size() );

  for ( const auto&& [tk, gtk] : Ranges::ConstZip(tracks, gtracks) ) {
    // Predict track
    const auto track_pred_values = predict_track( tk, rich_cnn_model );

    // Create a PID for this track
    auto* pid = new LHCb::RichPID();
    rPIDs.insert( pid, tk->key() );

    // Flag as an Offline GlobalPID result. Bit of a relic but useful
    // in comparisons to the 'current' stack.
    pid->setOfflineGlobal( true );

    // Set the track smart ref
    pid->setTrack( tk );

    // Set threshold info
    for ( const auto hypo : activeParticles() ) {
      pid->setAboveThreshold( hypo, gtk.thresholds()[hypo] );
    } // radiator flags
    pid->setUsedAerogel( gtk.radiatorActive()[Rich::Aerogel] );
    pid->setUsedRich1Gas( gtk.radiatorActive()[Rich::Rich1Gas] );
    pid->setUsedRich2Gas( gtk.radiatorActive()[Rich::Rich2Gas] );

    // Set best prediction
    Rich::ParticleIDType bestH      = Rich::ParticleIDType::Pion;
    auto                 best_value = 0.f;
    for ( const auto hypo : activeParticles() ) {
      // std::cout << "Active particle: " << hypo << "\n";
      const auto pred = track_pred_values[hypo];
      if ( pred > best_value ) {
        best_value = pred;
        bestH      = hypo;
      }
    }
    std::cout << "Best hypothesis for track " << tk->key() << " is " << bestH << " (" << best_value << ")\n";
    
    // Best PID value
    pid->setBestParticleID( bestH );
  }

  /* Restore the previous FPE flags*/
  feenableexcept( fpeflags );

  // return the PID objects
  return rPIDs;
}

PyObject* RichCNNSolver::fetch_model( const std::string& csv_event_location ) const {
  // Include module
  const auto module_name = PyUnicode_FromString( "RichCNNModel" );
  const auto module      = PyImport_Import( module_name );

  // Load CSVEventModel with a predefined location
  const auto csv_model      = PyObject_GetAttrString( module, "CSVEventModel" );
  const auto rich_cnn_model = PyObject_CallFunction( csv_model, "s", csv_event_location.c_str() );

  return rich_cnn_model;
}

std::vector<float> RichCNNSolver::predict_track( const LHCb::Track* track, PyObject* model ) const {
  // Now run the CNN model with track numbers
  const auto track_classification = PyObject_CallFunction( model, "i", static_cast<int>( track->key() ) );

  // Get the classification out of the track_classification variable
  const auto track_classification_list_size = PyList_Size( track_classification );
  // Assert we get a classification for each PID
  assert( track_classification_list_size == activeParticles().size() &&
          "The prediction values should cover all active particles" );

  std::vector<float> prediction_values;
  for ( int i = 0; i < track_classification_list_size; ++i ) {
    const auto item = PyList_GetItem( track_classification, i );
    prediction_values.push_back( static_cast<float>( PyFloat_AsDouble( item ) ) );
    // printf( "Value %i: %f\n", i, prediction_values.back() );
  }

  return prediction_values;
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RichCNNSolver )

//-----------------------------------------------------------------------------
