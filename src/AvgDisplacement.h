
#pragma once

// STD
#include <algorithm>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Event model
#include "Event/MCRichDigitSummary.h"

// Relations
#include "RichFutureMCUtils/RichRecMCHelper.h"

#include "RichFutureRecEvent/RichRecSpacePoints.h"
#include "RichInterfaces/IRichSmartIDTool.h"
#include <fstream>
#include <sys/stat.h>

// Relations
#include "RichFutureMCUtils/RichRecMCHelper.h"
#include "RichRecUtils/RichRadCorrLocalPositions.h"

#include <array>
#include <bitset>
#include <map>
#include <vector>

#include "Common.h"

// Use the functional framework
using namespace Gaudi::Functional;
using namespace Rich::Future::Rec;

namespace Rich::CNN {

  class AvgDisplacement final : public Consumer<void( const Rich::Future::Rec::Relations::TrackToSegments::Vector&, //
                                                      const SegmentPanelSpacePoints::Vector&,                       //
                                                      const LHCb::RichTrackSegment::Vector& )> {
  private:
    mutable std::map<Rich::DetectorType, std::vector<std::array<double, 2>>> distances;

  public:
    /// Standard constructor
    AvgDisplacement( const std::string& name, ISvcLocator* pSvcLocator );

    /// Algorithm initialization
    StatusCode initialize() override;

    /// Algorithm finalization
    StatusCode finalize() override;

    /// Functional operator
    void operator()( const Rich::Future::Rec::Relations::TrackToSegments::Vector&, //
                     const SegmentPanelSpacePoints::Vector&,                       //
                     const LHCb::RichTrackSegment::Vector& ) const override;
  };

} // namespace Rich::CNN
