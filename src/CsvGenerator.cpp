
// local
#include "CsvGenerator.h"
#include <utility>

using namespace Rich::CNN;

//-----------------------------------------------------------------------------
// Implementation file for class : CsvGenerator
//
// 2016-12-06 : Chris Jones
//-----------------------------------------------------------------------------

CsvGenerator::CsvGenerator( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"TrackToSegmentsLocation", Rich::Future::Rec::Relations::TrackToSegmentsLocation::Selected},
                 KeyValue{"TrackLocalPointsLocation", SpacePointLocation::SegmentsLocal},
                 KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                 KeyValue{"RichPixelGlobalPositionsLocation", SpacePointLocation::PixelsGlobal},
                 KeyValue{"RichPixelLocalPositionsLocation", SpacePointLocation::PixelsLocal},
                 KeyValue{"RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default},
                 KeyValue{"TracksLocation", LHCb::TrackLocation::Default},
                 KeyValue{"TrackToMCParticlesRelations", Rich::Future::MC::Relations::TrackToMCParticles},
                 KeyValue{"RichDigitSummariesLocation", LHCb::MCRichDigitSummaryLocation::Default}} ) {

  // Write out to files
  pre            = "/afs/cern.ch/work/d/dcampora/projects/csv_output/";
  current_folder = Common::first_free_index( "/afs/cern.ch/work/d/dcampora/projects/csv_output/", "" );
  extension      = ".csv";
  mkdir( ( pre ).c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

  ofs = std::map<std::string, std::ofstream*>( {{"rich1_top", nullptr},
                                                {"rich1_bottom", nullptr},
                                                {"rich2_left", nullptr},
                                                {"rich2_right", nullptr},
                                                {"segments", nullptr},
                                                {"mc_particles", nullptr},
                                                {"mc_pixel_to_particle", nullptr},
                                                {"mc_track_to_particle", nullptr},
                                                {"mc_true_cherenkov_photon", nullptr}} );

  std::string preface_rich = "pixel ID,PDCol,PDNumInCol,pixelCol,pixelRow,global x,global y,global z,local x,local "
                             "y,local z,corrected local x,corrected local y";
  preface = std::map<std::string, std::string>(
      {{"rich1_top", preface_rich},
       {"rich1_bottom", preface_rich},
       {"rich2_left", preface_rich},
       {"rich2_right", preface_rich},
       {"segments", "segment ID,track ID,RICH,radiator,middle point x,middle point y,middle point z,middle momentum x,\
middle momentum y,middle momentum z,average photon energy,topleft x,topleft y,topleft z,bottomright x,bottomright y,bottomright z,\
entry point x,entry point y,entry point z,exit point x,exit point y,exit point z"},
       {"mc_particles", "MC particle index,PID"},
       {"mc_pixel_to_particle", "pixel ID,MC particle index"},
       {"mc_track_to_particle", "track ID,MC particle index"},
       {"mc_true_cherenkov_photon", "track ID,pixel ID,MC particle index"}} );
}

void CsvGenerator::open_ofstreams() const {
  auto str_current_folder = pre + std::to_string( current_folder ) + "/";
  for ( auto& item : ofs ) { item.second = new std::ofstream( str_current_folder + item.first + extension ); }
}

void CsvGenerator::close_ofstreams() const {
  for ( auto& item : ofs ) {
    item.second->close();
    // Ask the internet
    delete item.second;
    item.second = 0;
  }
}

void CsvGenerator::write_preface() const {
  for ( auto& item : ofs ) { *( item.second ) << preface[item.first] << "\n"; }
}

std::ofstream* CsvGenerator::rich_ofstream( const Rich::DetectorType& type, const Rich::Side& side ) const {
  if ( type == Rich::Rich1 ) {
    if ( side == Rich::top ) {
      return ofs["rich1_top"];
    } else {
      return ofs["rich1_bottom"];
    }
  } else {
    if ( side == Rich::left ) {
      return ofs["rich2_left"];
    } else {
      return ofs["rich2_right"];
    }
  }
}

size_t CsvGenerator::fetch_key( std::vector<const LHCb::MCParticle*>& MCPs, const LHCb::MCParticle* MCP ) const {
  auto f = std::find( MCPs.begin(), MCPs.end(), MCP );
  if ( f != MCPs.end() ) {
    return std::distance( MCPs.begin(), f );
  } else {
    MCPs.push_back( MCP );
    return ( MCPs.size() - 1 );
  }
}

void CsvGenerator::operator()( const Rich::Future::Rec::Relations::TrackToSegments::Vector& tkToSegRels,
                               const SegmentPanelSpacePoints::Vector&                       trHitPntsLoc,
                               const LHCb::RichTrackSegment::Vector& segments, const SpacePointVector& globalPoints,
                               const SpacePointVector& localPoints, const Rich::PDPixelCluster::Vector& clusters,
                               const LHCb::Track::Selection&                   tracks,
                               const Rich::Future::MC::Relations::TkToMCPRels& tkrels,
                               const LHCb::MCRichDigitSummarys&                digitSums ) const {

  // Open new files for this event
  mkdir( ( pre + std::to_string( current_folder ) ).c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
  open_ofstreams();
  write_preface();

  // Fetch Monte Carlo helper
  Rich::Future::Rec::MC::Helper mcHelper( tkrels, digitSums );

  // Generate the pixel vector with all displaced pixels
  std::vector<std::array<double, 2>> displacedPixels = Common::get_displaced_pixels( localPoints, clusters );

  // Iterate over all segments
  for ( const auto& inTkRel : tkToSegRels ) {
    for ( const auto& segIndex : inTkRel.segmentIndices ) {
      // Rich::RichTrackSegment
      const auto& segment         = segments[segIndex];
      const auto& middlePoint     = segment.middlePoint();
      const auto& middleMomentum  = segment.middleMomentum();
      const auto& avPhotonEnergy  = segment.avPhotonEnergy();
      const auto& tkLocPtn        = trHitPntsLoc[segIndex];
      const auto& segPanelPntZero = tkLocPtn.point( Rich::top );
      const auto& segPanelPntOne  = tkLocPtn.point( Rich::bottom );
      const auto entryPoint       = segment.entryPoint();
      const auto exitPoint        = segment.exitPoint();

      *ofs["segments"] << segIndex << "," << inTkRel.tkIndex << "," << segment.rich() << "," << segment.radiator()
                       << "," << middlePoint.x() << "," << middlePoint.y() << "," << middlePoint.z() << ","
                       << middleMomentum.x() << "," << middleMomentum.y() << "," << middleMomentum.z() << ","
                       << avPhotonEnergy << "," << segPanelPntZero.x() << "," << segPanelPntZero.y() << ","
                       << segPanelPntZero.z() << "," << segPanelPntOne.x() << "," << segPanelPntOne.y() << ","
                       << segPanelPntOne.z() << "," << entryPoint.x() << "," << entryPoint.y() << "," << entryPoint.z() << ","
                       << exitPoint.x() << "," << exitPoint.y() << "," << exitPoint.z() << "\n";
    }
  }

  for ( size_t i = 0; i < clusters.size(); ++i ) {
    const auto& cluster        = clusters[i];
    const auto& gloPos         = globalPoints[i];
    const auto& locPos         = localPoints[i];
    const auto& displacedPixel = displacedPixels[i];

    auto smartID    = cluster.primaryID();
    auto ofs_output = rich_ofstream( smartID.rich(), smartID.panel() );

    *ofs_output << i
                << ","
                // PD
                << smartID.pdCol() << "," << smartID.pdNumInCol()
                << ","
                // Pixel
                << smartID.pixelCol() << "," << smartID.pixelRow()
                << ","
                // Global, local and local corrected positions
                << gloPos.x() << "," << gloPos.y() << "," << gloPos.z() << "," << locPos.x() << "," << locPos.y() << ","
                << locPos.z() << "," << displacedPixel[0] << "," << displacedPixel[1] << "\n";
  }

  // Print for each cluster, the associated mc particles
  std::vector<const LHCb::MCParticle*> MCPs;
  for ( unsigned i = 0; i < clusters.size(); ++i ) {
    auto& cluster = clusters[i];
    // Get MCParticles for the cluster
    const auto mcParts = mcHelper.mcParticles( cluster );
    // Loop over all MCParticles associated to the pixel
    for ( const auto* MCP : mcParts ) {
      int key = fetch_key( MCPs, MCP );
      *ofs["mc_pixel_to_particle"] << i << "," << key << "\n";
    }
  }

  // Print for each track, the associated particle(s)
  for ( unsigned i = 0; i < tracks.size(); ++i ) {
    // Get the MCParticles for this track
    const auto associated_mcps = mcHelper.mcParticles( *( tracks[i] ), true, 0.5 );
    if ( associated_mcps.empty() ) {
      *ofs["mc_track_to_particle"] << i << ",-1" << "\n";
    } else {
      // loop over MCPs
      for ( auto* MCP : associated_mcps ) {
        int key = fetch_key( MCPs, MCP );
        *ofs["mc_track_to_particle"] << i << "," << key << "\n";
      }
    }
  }

  for ( unsigned i = 0; i < tracks.size(); ++i ) {
    for ( unsigned j = 0; j < clusters.size(); ++j ) {
      const auto rad = clusters[j].rich() == Rich::Rich1 ? Rich::Rich1Gas : Rich::Rich2Gas;
      const auto associated_mcps = mcHelper.trueCherenkovPhoton(*(tracks[i]), rad, clusters[j]);
      for ( auto* MCP : associated_mcps ) {
        int key = fetch_key( MCPs, MCP );
        *ofs["mc_true_cherenkov_photon"] << i << "," << j << "," << key << "\n";
      }

      // // Rich::Rich1Gas
      // // Rich::Rich2Gas
      // clusters[j].rich() // Rich::DetectorType : Rich::Rich1, Rich::Rich2
    }
  }

  // LHCb::MCParticle::ConstVector trueCherenkovPhoton( const LHCb::Track&          track, //
  //                                                      const Rich::RadiatorType    rad,   //
  //                                                      const Rich::PDPixelCluster& cluster ) const;

  // Write out MC particles
  for ( unsigned i = 0; i < MCPs.size(); ++i ) {
    auto* MCP = MCPs[i];
    *ofs["mc_particles"] << i << "," << mcHelper.mcParticleType( MCP ) << "\n";
  }

  // Close files and increment folder number
  close_ofstreams();
  current_folder++;
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CsvGenerator )

//-----------------------------------------------------------------------------
