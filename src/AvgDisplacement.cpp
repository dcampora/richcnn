
// local
#include "AvgDisplacement.h"
#include <utility>

using namespace Rich::CNN;

//-----------------------------------------------------------------------------
// Implementation file for class : AvgDisplacement
//
// 2016-12-06 : Chris Jones
//-----------------------------------------------------------------------------

AvgDisplacement::AvgDisplacement( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"TrackToSegmentsLocation", "Rec/Rich/Relations/TrackToSegments/Selected/Long"},
                 KeyValue{"TrackLocalPointsLocation", "Rec/Rich/SegmentPositions/Local/Long"},
                 KeyValue{"TrackSegmentsLocation", "Rec/Rich/TrackSegments/Long"}} ) {}

StatusCode AvgDisplacement::initialize() {
 // Sets up various tools and services
  auto sc = Consumer::initialize();
  if ( !sc ) return sc;

  distances[Rich::Rich1] = std::vector<std::array<double, 2>>();
  distances[Rich::Rich2] = std::vector<std::array<double, 2>>();

  return StatusCode::SUCCESS;
}

StatusCode AvgDisplacement::finalize() {
  if ( distances[Rich::Rich1].size() > 0 ) {
    auto                  size          = distances[Rich::Rich1].size();
    std::array<double, 2> average_rich1 = {0.0, 0.0};

    for ( auto& v : distances[Rich::Rich1] ) {
      average_rich1[0] += v[0];
      average_rich1[1] += v[1];
    }

    std::cout << "Average distances RICH 1: " << ( average_rich1[0] / size ) << ", " << ( average_rich1[1] / size )
              << std::endl;
  }

  if ( distances[Rich::Rich2].size() > 0 ) {
    auto                  size          = distances[Rich::Rich2].size();
    std::array<double, 2> average_rich2 = {0.0, 0.0};

    for ( auto& v : distances[Rich::Rich2] ) {
      average_rich2[0] += v[0];
      average_rich2[1] += v[1];
    }

    std::cout << "Average distances RICH 2: " << ( average_rich2[0] / size ) << ", " << ( average_rich2[1] / size )
              << std::endl;
  }

  return Consumer::finalize();
}

void AvgDisplacement::operator()( const Rich::Future::Rec::Relations::TrackToSegments::Vector& tkToSegRels,
                                  const SegmentPanelSpacePoints::Vector&                       trHitPntsLoc,
                                  const LHCb::RichTrackSegment::Vector&                        segments ) const {
  // Iterate over all segments
  for ( const auto& inTkRel : tkToSegRels ) {
    for ( const auto& segIndex : inTkRel.segmentIndices ) {
      // Rich::RichTrackSegment
      const auto& segment  = segments[segIndex];
      const auto& tkLocPtn = trHitPntsLoc[segIndex];
      const auto& segPanelPntZero =
          Common::corrector.correct( tkLocPtn.point( Rich::top ), Common::gas[segment.rich()] );
      const auto& segPanelPntOne =
          Common::corrector.correct( tkLocPtn.point( Rich::bottom ), Common::gas[segment.rich()] );

      const std::array<double, 2> diffs = {segPanelPntZero.x() - segPanelPntOne.x(),
                                           segPanelPntZero.y() - segPanelPntOne.y()};

      if ( diffs[0] <= 300 and diffs[0] >= -300 and diffs[1] <= 300 and diffs[1] >= -300 ) {
        distances[segment.rich()].push_back( diffs );
      }
    }
  }
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( AvgDisplacement )

//-----------------------------------------------------------------------------
