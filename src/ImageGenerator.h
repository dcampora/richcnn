
#pragma once

// STD
#include <algorithm>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Event model
#include "Event/MCRichDigitSummary.h"

// Relations
#include "RichFutureMCUtils/RichRecMCHelper.h"

#include "RichFutureRecEvent/RichRecSpacePoints.h"
#include "RichInterfaces/IRichSmartIDTool.h"
#include <fstream>
#include <sys/stat.h>

// Relations
#include "RichFutureMCUtils/RichRecMCHelper.h"
#include "RichRecUtils/RichRadCorrLocalPositions.h"

#include <array>
#include <bitset>
#include <map>
#include <vector>

#include "Common.h"

// Use the functional framework
using namespace Gaudi::Functional;
using namespace Rich::Future::Rec;

namespace Rich::CNN {

  class ImageGenerator final : public Consumer<void( const Rich::Future::Rec::Relations::TrackToSegments::Vector&, //
                                                     const SegmentPanelSpacePoints::Vector&,                       //
                                                     const LHCb::RichTrackSegment::Vector&,                        //
                                                     const SpacePointVector&,                                      //
                                                     const Rich::PDPixelCluster::Vector&,                          //
                                                     const LHCb::Track::Selection&,                                //
                                                     const Rich::Future::MC::Relations::TkToMCPRels&,              //
                                                     const LHCb::MCRichDigitSummarys& )> {
  private:
    std::string pre, extension;

    // Desired parameters
    const int image_size{64};
    const int max_elements_per_file{40000};

    // Generate separate MCP noise or not
    const bool generate_separate_mcp_noise{false};

    mutable std::map<Rich::ParticleIDType, uint8_t>              pid_associations;
    mutable std::map<double, std::map<std::string, std::string>> files;
    mutable std::map<double, std::ofstream>                      x_descriptors;
    mutable std::map<double, std::ofstream>                      n_descriptors;
    mutable std::map<double, std::ofstream>                      p_descriptors;
    mutable std::map<double, std::ofstream>                      y_descriptors;

    std::array<double, 2>   min_max_momenta;
    std::array<double, 144> momenta_ranges;

  private:
    std::ofstream get_ofstream( const std::string& filename ) const;

    void increment_file_counter( const std::array<double, 6>& mrange ) const;

    size_t fetch_key( std::vector<const LHCb::MCParticle*>& MCPs,         //
                      std::vector<std::vector<size_t>>&     mcp_clusters, //
                      const LHCb::MCParticle*               MCP ) const;

    void print_image( const std::array<uint64_t, 64>& image_pixels ) const;

  public:
    /// Standard constructor
    ImageGenerator( const std::string& name, ISvcLocator* pSvcLocator );

    /// Algorithm initialization
    StatusCode initialize() override;

    /// Algorithm finalization
    StatusCode finalize() override;

    /// Functional operator
    void operator()( const Rich::Future::Rec::Relations::TrackToSegments::Vector&, //
                     const SegmentPanelSpacePoints::Vector&,                       //
                     const LHCb::RichTrackSegment::Vector&,                        //
                     const SpacePointVector&,                                      //
                     const Rich::PDPixelCluster::Vector&,                          //
                     const LHCb::Track::Selection&,                                //
                     const Rich::Future::MC::Relations::TkToMCPRels&,              //
                     const LHCb::MCRichDigitSummarys& ) const override;
  };

} // namespace Rich::CNN
